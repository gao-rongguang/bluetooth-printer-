#include "led.h"


/*****************************
函数功能：LED灯初始化
说明：PA1----低电平有效
******************************/

void LED_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct; //定义结果体变量
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);    //开A口时钟


	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;  //选中管脚1
	GPIO_InitStruct.GPIO_Mode =GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度	
	GPIO_Init(GPIOA,&GPIO_InitStruct);

	
	GPIO_SetBits(GPIOA,GPIO_Pin_1); //配置初始状态  高电平
}