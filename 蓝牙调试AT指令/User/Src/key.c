#include "key.h"
#include "time.h"

/******************************
函数功能：按键初始化
说明：PB8----低电平有效
*****************************/

void KEY_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;                   //定义结构体变量
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE );   //使能B口时钟	
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;    //浮空输入
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;          //选中管脚8
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	GPIO_SetBits(GPIOB,GPIO_Pin_8);              //配置初始状态  高电平
}

/*******************按键扫描函数*****************************************/
u8 KEY_Scan(void)
{
	static u8 key = 0;  //按键状态标志位  0：允许操作 1：不允许操作
	if((key == 0)&&(!(GPIOB->IDR &1<<8)))
	{
		TIM3_ms(10);   //消抖
		if(!(GPIOB->IDR &1<<8))
		{
			key = 1;   //不允许操作按键
			return 1;
		}
	}
	else if((key == 1)&&(GPIOB->IDR &1<<8))  //松手检测
	{
		key = 0;   //按键允许操作
	}
	return 0;
}

