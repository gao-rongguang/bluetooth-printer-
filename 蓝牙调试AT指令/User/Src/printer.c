#include "printer.h"
#include "time.h"



/*******************************
函数功能：打印头初始化
说明：
STB1-6----PB15-10  输出
LAT-------PA8
CLK-------PA11    输入
DI--------PA12    输入

*********************************/
void Printer_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct; //定义结果体变量
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);    //开A口时钟  B口时钟


	GPIO_InitStruct.GPIO_Pin =GPIO_Pin_8 ;  //选中管脚10 11  12
	GPIO_InitStruct.GPIO_Mode =GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度	
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12;  //选中管脚10 11  12
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING; //浮空输入	
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 
	                           | GPIO_Pin_14 | GPIO_Pin_15;  //选中管脚10 11  12  13 14 15
	GPIO_InitStruct.GPIO_Mode =GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度	
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
}

void Printer_time()
{
	//初始状态
	CLK_L;
	DI_L;
	LAT_H;
	STB1_L;
	STB2_L;
	STB3_L;
	STB4_L;
	STB5_L;
	STB6_L;
	TIM3_us(1);
	//
	DI_H;
	TIM3_us_xiao(0.03);  //延时最低30ns
	CLK_H;
	TIM3_us_xiao(0.04);  //延时最低30ns
	DI_L;
	LAT_L;
	TIM3_us_xiao(0.04);  //延时最低30ns
	CLK_L;
	TIM3_us_xiao(0.07);  //延时最低30ns
	LAT_H;
	TIM3_us_xiao(0.3);  //延时最低300ns
	CLK_H;
	STB1_H;
	STB2_H;
	STB3_H;
	STB4_H;
	STB5_H;
	STB6_H;
	TIM3_us(800);
	
	//结束
	CLK_L;
	DI_L;
	LAT_H;
	STB1_L;
	STB2_L;
	STB3_L;
	STB4_L;
	STB5_L;
	STB6_L;
	
	
	
}
