#include "lb1936.h"
#include "time.h"

/*******************************
函数功能：步进电机初始化
说明：
PB3 PB4 PB5 PB6----
*********************************/
void LB1936_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;                   //定义结构体变量
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE );   //使能B口时钟	
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;    //输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度50MHz
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 |GPIO_Pin_5 |GPIO_Pin_6;          //选中管脚3 4 5 6
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;          //选中管脚4
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;          //选中管脚5
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;          //选中管脚6
	GPIO_Init(GPIOB,&GPIO_InitStruct);
}


void SiPai()
{
	//初始状态
	LB1_H;
	LB2_L;
	LB3_L;
	LB4_L;
	TIM3_us(900);
	
	
	//一拍
	LB1_H;
	LB2_L;
	LB3_L;
	LB4_H;
	TIM3_us(900);
	//2拍
	LB1_L;
	LB2_H;
	LB3_L;
	LB4_H;
	TIM3_us(900);
	//3拍
	LB1_L;
	LB2_H;
	LB3_H;
	LB4_L;
	TIM3_us(900);
	//4拍
	LB1_H;
	LB2_L;
	LB3_H;
	LB4_L;
	TIM3_us(900);
	
	
}
	

	
	
	