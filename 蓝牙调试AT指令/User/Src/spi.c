#include "spi.h"

/**********************************************
函数功能：SPI初始化函数
硬件接口：
MOSI---------PA7---------推挽复用
MISO---------PA6---------浮空/带上拉输入
SCLK---------PA5---------推挽复用
C  S---------PA4---------推挽输出
**********************************************/

void SPI_W25Q64_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	SPI_InitTypeDef  SPI_InitStruct;
	//GPIO口
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 | RCC_APB2Periph_GPIOA ,ENABLE);//开时钟
	//PA7/PA5
	GPIO_InitStruct.GPIO_Pin =GPIO_Pin_5 | GPIO_Pin_7 ;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	//PA6
	GPIO_InitStruct.GPIO_Pin =GPIO_Pin_6 ;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;  //浮空输入
	GPIO_Init(GPIOA,&GPIO_InitStruct);	
	
	//PB0
	GPIO_InitStruct.GPIO_Pin =GPIO_Pin_4;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);	
	//SPI
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex; //全双工模式
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;  //主机模式
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;  //8位数据位
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;  //时钟初始电平为低电平
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;   //时钟线第一个边沿采集
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;  //片选信号软件管理
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;  //4分频
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;  //先发高位
	SPI_Init(SPI1,&SPI_InitStruct);
	
	//设置片选位拉高 PB0
	GPIO_SetBits(GPIOB, GPIO_Pin_0);

	SPI_Cmd(SPI1,ENABLE );  //使能SPI1
}


/***************************************
函数功能：SPI1读写函数
函数参数：
要发送的数据
函数返回值：
接收到的数据
***************************************/
u16 SPI_Read_Write_Byte(u16 data)
{
	while(!SPI_I2S_GetFlagStatus(SPI1,SPI_I2S_FLAG_TXE))
	{
		//轮询发送缓冲区是否为空
	}
	SPI_I2S_ClearFlag(SPI1,SPI_I2S_FLAG_TXE);  //清标志位
	SPI_I2S_SendData(SPI1,data);  //发送数据
	while(!SPI_I2S_GetFlagStatus(SPI1,SPI_I2S_FLAG_RXNE))
	{
		//轮询接收缓冲区是否非空
	}
	SPI_I2S_ClearFlag(SPI1,SPI_I2S_FLAG_RXNE);  //清标志位
	return SPI_I2S_ReceiveData(SPI1);
}


















