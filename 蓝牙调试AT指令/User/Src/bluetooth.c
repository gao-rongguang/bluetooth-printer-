#include "bluetooth.h"
#include "stdio.h"

//void BLUE_Init(void)
//{
//	GPIO_InitTypeDef GPIO_InitStruct;
//	
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
//	
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_9;
//	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
//	GPIO_Init(GPIOB, &GPIO_InitStruct);
//	
//	GPIO_SetBits(GPIOB, GPIO_Pin_1);
//	GPIO_SetBits(GPIOB, GPIO_Pin_9);
//}



/***********************************
函数功能：蓝牙初始化
说明：PA2-----TX-----推挽复用输出
      PA3-----RX-----浮空输入或带上拉输入
************************************/
void USART2_Init(u32 Bount)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;  // 定义结构体变量
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA  ,ENABLE);  //开A口时钟  
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);    //串口2时钟
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;     //引脚2
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;  //推挽复用输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;  //输出速率为50MHZ
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3;     //引脚3
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;  //上拉输入
	GPIO_Init(GPIOA,&GPIO_InitStruct);	
	
	//USART设置
	USART_InitStruct.USART_BaudRate = Bount;  //波特率
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;  //数据位8位
	USART_InitStruct.USART_StopBits = USART_StopBits_1;     //一个停止位
	USART_InitStruct.USART_Parity = USART_Parity_No;        //无奇偶模式
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;    //无硬件流控制
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;       //使能发送  接收
	USART_Init(USART2,&USART_InitStruct);

	//NVIC设置
	NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;            //设置中断源串口1
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;     //抢占优先级
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;           //响应优先级
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;            //使能NVIC
	NVIC_Init(&NVIC_InitStruct);
	
	USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);            //使能串口2中断

	//USART_DMACmd(USART2,USART_DMAReq_Rx, ENABLE );       //使能接收DMA请求
	USART_Cmd(USART2,ENABLE);  //使能串口2
}	

/********************** 串口中断服务函数 ************************************/
u8 receive_buff[20];   //串口2接收的数据
u8 receive_len = 0;   //串口2接收的数据的长度
u8 receive_flag = 0;
void USART2_IRQHandler(void)
{
	
	if(USART_GetITStatus(USART2,USART_IT_RXNE))
	{
		USART_ClearITPendingBit(USART2,USART_IT_RXNE);   //清标志
		
		TIM_Cmd(TIM2,DISABLE );  //关闭定时器2
		TIM_SetCounter(TIM2,0);   //计数器置0
		receive_buff[receive_len] = USART_ReceiveData(USART2);
		receive_len++;
		
		if(receive_len >= 20)
		{
			receive_len = 0;
			
		}
		
		TIM_Cmd(TIM2,ENABLE );  //开启定时器2
		
	}
	
}

/***************** 串口发送字符串函数 **************************/
void USART_2_Send_Str(u8 Data)
{
//	while( Data != '\0')
//	{
		
		while( !USART_GetFlagStatus(USART2,USART_FLAG_TC));       //发送完成
		USART_SendData(USART2,Data);
//		Data++;
//	}
}
void USART_12_Send_Str(char *Data)
{
	while( *Data != '\0')
	{
		USART_2_Send_Str(*Data);
		//while( !USART_GetFlagStatus(USART2,USART_FLAG_TC));       //发送完成
		Data++;
	}
	USART_2_Send_Str('\r');
	USART_2_Send_Str('\n');
	
}


/******************************
函数功能：blue key初始化
说明：PB9---高电平有效
*****************************/
void BLUE_KEY()
{
	GPIO_InitTypeDef GPIO_InitStruct;                   //定义结构体变量
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE );   //使能B口时钟	
	
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;    //浮空输入
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;    //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;  //输出速率为50MHZ
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;          //选中管脚9
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	GPIO_SetBits(GPIOB,GPIO_Pin_9);              //配置初始状态  高电平
	//GPIO_ResetBits(GPIOB,GPIO_Pin_9);
}


/******************************
函数功能：blue pwr初始化
说明：PB1-----高电平有效
*****************************/
void BLUE_PWR()
{
	GPIO_InitTypeDef GPIO_InitStruct;                   //定义结构体变量
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE );   //使能B口时钟	
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;    //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;  //输出速率为50MHZ
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;          //选中管脚1
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	GPIO_SetBits(GPIOB,GPIO_Pin_1);              //配置初始状态  高电平
	//GPIO_ResetBits(GPIOB,GPIO_Pin_1);
}
