#ifndef __LB1936_H__
#define __LB1936_H__

#include "stm32f10x.h"
#include "io_bit.h"



#define LB1_H GPIO_SetBits(GPIOB,GPIO_Pin_3)
#define LB1_L GPIO_ResetBits(GPIOB,GPIO_Pin_3)
#define LB2_H GPIO_SetBits(GPIOB,GPIO_Pin_4)
#define LB2_L GPIO_ResetBits(GPIOB,GPIO_Pin_4)
#define LB3_H GPIO_SetBits(GPIOB,GPIO_Pin_5)
#define LB3_L GPIO_ResetBits(GPIOB,GPIO_Pin_5)
#define LB4_H GPIO_SetBits(GPIOB,GPIO_Pin_6)
#define LB4_L GPIO_ResetBits(GPIOB,GPIO_Pin_6)

void LB1936_Init();
void SiPai();
void M_Drive(void);







#endif