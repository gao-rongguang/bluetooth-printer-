#ifndef __W25Q64_H__
#define __W25Q64_H__

#include "stm32f10x.h"



#define CS_L  GPIO_ResetBits(GPIOA, GPIO_Pin_4) 
#define CS_H  GPIO_SetBits(GPIOA, GPIO_Pin_4) 



u16 W25Q64_ID();
void Read_data(u32 add,u32 num,u8 *buff);
void W25Q64_Write_Page(u32 Write_Add,u16 Write_Number,u8 *Write_Buff);
void W25Q64_Spread_Page_Write(u32 Write_Add,u16 Write_Number,u8 *Write_Buff);
void W25Q64_Write_Enable(void);
u8 W25Q64_Read_Status(void);
void W25Q64_Wait_Busy(void);
void W25Q64_Chip_Erase(void);
void W25Q64_Sector_Erase(u32 Sec_Num);
void Wait_busy();


void CE(void);





#endif






//#define __FLASH_H__
//#ifndef __FLASH_H__

//#endif

//#include "stm32f10x.h"

//u16 flashID(void);
//void WE(void);
//u8 RSR(void);
//void WBusy(void);
//void RD(u16 RN,u32 RD,u8 *RB);
//void SE(u32 dz);
//void RBusy(void);
//void CE(void);
//void WP(u16 WN,u32 WD,u8 *WB);



