#ifndef __PRINTER_H__
#define __PRINTER_H__

#include "stm32f10x.h"
#include "io_bit.h"

#define DI  PAout(12)

#define LAT_H GPIO_SetBits(GPIOA,GPIO_Pin_8)
#define LAT_L GPIO_ResetBits(GPIOA,GPIO_Pin_8)
#define CLK_H GPIO_SetBits(GPIOA,GPIO_Pin_11)
#define CLK_L GPIO_ResetBits(GPIOA,GPIO_Pin_11)
#define DI_H GPIO_SetBits(GPIOA,GPIO_Pin_12)
#define DI_L GPIO_ResetBits(GPIOA,GPIO_Pin_12)

#define STB1_H GPIO_SetBits(GPIOB,GPIO_Pin_15)
#define STB1_L GPIO_ResetBits(GPIOB,GPIO_Pin_15)
#define STB2_H GPIO_SetBits(GPIOB,GPIO_Pin_14)
#define STB2_L GPIO_ResetBits(GPIOB,GPIO_Pin_14)
#define STB3_H GPIO_SetBits(GPIOB,GPIO_Pin_13)
#define STB3_L GPIO_ResetBits(GPIOB,GPIO_Pin_13)
#define STB4_H GPIO_SetBits(GPIOB,GPIO_Pin_12)
#define STB4_L GPIO_ResetBits(GPIOB,GPIO_Pin_12)
#define STB5_H GPIO_SetBits(GPIOB,GPIO_Pin_11)
#define STB5_L GPIO_ResetBits(GPIOB,GPIO_Pin_11)
#define STB6_H GPIO_SetBits(GPIOB,GPIO_Pin_10)
#define STB6_L GPIO_ResetBits(GPIOB,GPIO_Pin_10)


void Printer_Init();
void Lat_data(void);
void Printer_off();
void Printer_left();
void Printer_right();
void write_data_8bit(u8 dat);
void Printer_hz();
void Printer_tp();
void Printer_hz_bule() ;
void Word();
void Painter(void);
void PaintH(u8 *data);
void PaintClearHZ(void);
void PaintZF(u8 data);
void PainterT(void);



extern u8 Printer_flag;
extern u8 buff[12*128];








#endif
