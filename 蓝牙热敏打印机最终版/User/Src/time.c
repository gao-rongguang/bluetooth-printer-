#include "time.h"
#include "bluetooth.h"
#include "stdio.h"
#include "ch340.h"
/************************************************
函数功能：定时器2初始化函数
函数参数：
************************************************/
void TIM2_Init(u16 arr,u16 psc)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	
	TIM_TimeBaseInitStruct.TIM_Period = arr;   //重装载值
	TIM_TimeBaseInitStruct.TIM_Prescaler =psc-1;  //分频值
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseInitStruct);
	
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);  //使能中断
	
	//NVIC
	NVIC_InitStruct.NVIC_IRQChannel = TIM2_IRQn;  //定时器2中断
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;  //抢占优先级
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;  //响应优先级
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;   //通道使能
	NVIC_Init(&NVIC_InitStruct);
	TIM_Cmd(TIM2,DISABLE );  //失能模块
}

/************************中断服务函数**********************************/
int rel = 0;    //数据接收的长度
void TIM2_IRQHandler(void)
{
	int i = 0;

		if(TIM_GetITStatus(TIM2,TIM_IT_Update))   //检测是否是更新
		{
			
			TIM_ClearITPendingBit(TIM2,TIM_IT_Update);   //清标志
			if(mode == 0)
			{
				receive_buff[receive_len] = '\0';
				for(i = receive_len+1; i<20;i++)
				{
					receive_buff[i] = NULL;
				}
				if(receive_len >= 19)
				{	
					receive_buff[19] = '\0';
						
				}
				rel = receive_len;
				receive_len = 0;
				blue_flag1 = 1;	
			
				TIM_Cmd(TIM2,DISABLE );  //关闭定时器2
			}
			else if(mode == 1)
			{
				TIM_Cmd(TIM2,DISABLE );  //关闭定时器2
				rec_sta = 1;
				rec_num = 0;
			}
		}
	
}




/************************************************
函数功能：定时器3初始化函数
函数参数：
************************************************/
TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
void TIM3_Init()
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStruct);
	
	TIM_Cmd(TIM3,DISABLE);  //失能模块
}

void TIM3_ms(u16 arr)
{
	TIM_TimeBaseInitStruct.TIM_Period = arr*10;   //重装载值
	TIM_TimeBaseInitStruct.TIM_Prescaler =7200-1;  //分频值
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStruct);
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	TIM_Cmd(TIM3,ENABLE);  //使能模块
	
	while( !TIM_GetFlagStatus(TIM3,TIM_FLAG_Update))
	{
		//轮询状态标志位
	}
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	TIM_Cmd(TIM3,DISABLE);  //失能模块
}


void TIM3_us(u16 arr)
{
	TIM_TimeBaseInitStruct.TIM_Period = arr;   //重装载值
	TIM_TimeBaseInitStruct.TIM_Prescaler =72-1;  //分频值
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStruct);
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	TIM_Cmd(TIM3,ENABLE);  //使能模块
	
	while( !TIM_GetFlagStatus(TIM3,TIM_FLAG_Update))
	{
		
		//轮询状态标志位
	}
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	TIM_Cmd(TIM3,DISABLE);  //失能模块
}
void TIM3_us_xiao(float arr)
{
	TIM_TimeBaseInitStruct.TIM_Period = arr;   //重装载值
	TIM_TimeBaseInitStruct.TIM_Prescaler =72-1;  //分频值
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStruct);
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	TIM_Cmd(TIM3,ENABLE);  //使能模块
	
	while( !TIM_GetFlagStatus(TIM3,TIM_FLAG_Update))
	{
		
		//轮询状态标志位
	}
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	TIM_Cmd(TIM3,DISABLE);  //失能模块
}