#include "w25q64.h"
#include "spi.h"



u16 W25Q64_ID()
{
	u16 temp ; 
	CS_L;
	SPI_Read_Write_Byte(0x90);  //发送指令
	SPI_Read_Write_Byte(0x00);
	SPI_Read_Write_Byte(0x00);
	SPI_Read_Write_Byte(0x00);  //发送24位地址
	
	//接收数据 
	temp = SPI_Read_Write_Byte(0x11);
	temp = temp<< 8 | SPI_Read_Write_Byte(0x11);
	CS_H;
	return temp;
}


/**************************************
函数功能：读数据函数
函数参数：
add：读取数据的地址
num：读取的数据个数
*buff：输出参数---读取到的数据
****************************************/
void Read_data(u32 add,u32 num,u8 *buff)
{
	u32 i;
	CS_L;  //片选拉低进行通信 
	SPI_Read_Write_Byte(0x03);  //发送读数据指令
	SPI_Read_Write_Byte(add >> 16);  //发送高16位地址
	SPI_Read_Write_Byte(add >> 8 & 0xff);   //发送中间8位地址
	SPI_Read_Write_Byte(add & 0xff);  //发送地址低8位
	for(i = 0 ; i < num; i++)
	{
		*buff = SPI_Read_Write_Byte(0xFF);//读数据
		buff++;
	}
	CS_H; //读数据结束
}

/*********************************************************************
函数功能：页编程函数
函数参数：
Write_Add:要写入的地址
Write_Number:要写入的字节个数  (一页内最多写256字节)
*Write_Buff:要写入的内容的首地址
函数返回值：无
*********************************************************************/
void W25Q64_Write_Page(u32 Write_Add,u16 Write_Number,u8 *Write_Buff)
{  //0x123456  & 0xff0000 >>16
	u16 i;    //a = (b >> 2) + 12  b = 10
	W25Q64_Write_Enable();     //写使能
	CS_L;     //拉低片选
	
	SPI_Read_Write_Byte(0x02);   //发送页编程指令
	SPI_Read_Write_Byte(Write_Add >> 16);   //发送24位地址高8位
	SPI_Read_Write_Byte(Write_Add >> 8);    //发送24位地址中间8位
	SPI_Read_Write_Byte(Write_Add);         //发送24位地址低8位
	for(i = 0 ; i < Write_Number; i++)
	{
		SPI_Read_Write_Byte(Write_Buff[i]);   //写要发送的字节（1个）
	}
	CS_H;   //拉高片选
	W25Q64_Wait_Busy();   //等待写入数据结束
}
/************************ 跨页写函数 ************************
参数：
	add：器件地址
	byte_add:写数据的地址
	data :要写的数据          
    len：   数据数量  
功能：跨页写入 1 串数据   
*****************************************************************/
void W25Q64_Spread_Page_Write(u32 Write_Add,u16 Write_Number,u8 *Write_Buff)
{
	u16 surplus_bytes = 0;
	u16 write_bytes = 0;
	
    surplus_bytes  = 256 - Write_Add % 256;            //在开始页要写入的个数
	Write_Add = (Write_Number > surplus_bytes ) ? surplus_bytes : Write_Number;
	while(Write_Number)
	{
		W25Q64_Write_Page(Write_Add,write_bytes,Write_Buff);
		Write_Buff += write_bytes;
		Write_Add += write_bytes;
		Write_Number -= write_bytes;
		
		write_bytes = (Write_Number > 256)? 256: Write_Number;
	}
         
    
}
/***************************************************************
函数功能：写使能函数
函数参数：无
函数返回值：无
***************************************************************/
void W25Q64_Write_Enable(void)
{
	CS_L;   //拉低片选
	SPI_Read_Write_Byte(0x06);    //发送写使能指令
	CS_H;   //拉高片选
}

/****************************************************************
函数功能：读状态寄存器
函数参数：无
函数返回值：
	0：不忙
	1：忙
****************************************************************/
u8 W25Q64_Read_Status(void)
{
	u8 flag = 0;
	CS_L;   //拉低片选
	SPI_Read_Write_Byte(0x05);    //发送读状态指令
	flag = SPI_Read_Write_Byte(0xff);    //读取状态寄存器内容  括号里可以是任何数：因为SPI是一个数据交换协议
	CS_H;   //拉高片选
	return flag;  //0
}

/**************************************************************
函数功能：检测空闲函数
函数参数：无
函数返回值：无
**************************************************************/
void W25Q64_Wait_Busy(void)
{
	while(W25Q64_Read_Status())
	{
		//等待BUSY位清零
	}
}
/**************************
擦除等待
***************************/
void Wait_busy()
{
	while((W25Q64_Read_Status() & 0x01) == 1);

}
/*****************************************************************************
函数功能：芯片擦除函数
函数参数：无
函数返回值：无
*****************************************************************************/
void W25Q64_Chip_Erase(void)
{
	W25Q64_Write_Enable();    //写使能
	Wait_busy();
	CS_L;   //拉低片选
	SPI_Read_Write_Byte(0xC7);    //发送芯片擦除指令
	CS_H;   //拉高片选	
	Wait_busy();    //等待擦除完毕
}


/***************************************************************************
函数功能：扇区擦除函数
函数参数：
函数返回值：无
****************************************************************************/
void W25Q64_Sector_Erase(u32 Sec_Num)
{
	u32 Sec_Add = Sec_Num *4096;   //每个扇区的首地址  
	W25Q64_Write_Enable();    //写使能
	CS_L;   //拉低片选	
	SPI_Read_Write_Byte(0x20);    //发送扇区擦除指令
	SPI_Read_Write_Byte(Sec_Add >>16);    //发送需要擦除的扇区地址高8位
	SPI_Read_Write_Byte(Sec_Add >>8);    //发送需要擦除的扇区地址中间8位
	SPI_Read_Write_Byte(Sec_Add );    //发送需要擦除的扇区地址低8位
	CS_H;   //拉高片选	
	Wait_busy();    //等待擦除完毕	
}




