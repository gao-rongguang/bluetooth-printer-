#include "beep.h"
#include "time.h"

/*****************************
函数功能：蜂鸣器灯初始化
说明：PA0----高电平有效
******************************/

void BEEP_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct; //定义结果体变量
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);    //开A口时钟


	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;  //选中管脚1
	GPIO_InitStruct.GPIO_Mode =GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度	
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	BEEP = 0;   //关闭
}
/*****************************
函数功能：缺纸检测
说明：PB0----高电平有效
返回值：1 响
        0 不响
******************************/
int Paper_lack() 
{
	GPIO_InitTypeDef GPIO_InitStruct; //定义结果体变量
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);    //开A口时钟


	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;  //选中管脚1
	GPIO_InitStruct.GPIO_Mode =GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度	
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	GPIO_SetBits(GPIOB, GPIO_Pin_0);
	
	if(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_0) == 1)    //缺纸是读取到高电平
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

void beep_ring()
{
	 if(Paper_lack() == 1)    //缺纸检测
	{
		BEEP = 1;   
		TIM3_ms(1000);
		BEEP = 0; 
		TIM3_ms(5000);
	}
	else if(Paper_lack() == 0)
	{	
		BEEP = 0; 
	}
}