#include "lb1936.h"
#include "time.h"

/*******************************
函数功能：步进电机初始化
说明：
PB3 PB4 PB5 PB6----输出
*********************************/
void LB1936_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;                   //定义结构体变量
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE );   //使能B口时钟	
	
	RCC->APB2ENR |= (0X1<<0);//开启复用功能
	AFIO->MAPR &= ~(0x7<<24);//清零
	AFIO->MAPR |= (0X2<<24);//关闭JTAG-DP，启用SW-DP 释放PB3 PB4
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;    //输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度50MHz
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 |GPIO_Pin_5 |GPIO_Pin_6;          //选中管脚3 4 5 6
	GPIO_Init(GPIOB,&GPIO_InitStruct);

}

void M_Drive(void)   //电机时序驱动   四节拍
{
		LB1_H;
		LB2_L;
		LB3_L;
		LB4_H;
	TIM3_us(1600);
		LB1_L;
		LB2_H;
		LB3_L;
		LB4_H;
	TIM3_us(1600);
		LB1_L;
		LB2_H;
		LB3_H;
		LB4_L;	
	TIM3_us(1600);
		LB1_H;
		LB2_L;
		LB3_H;
		LB4_L;	
	TIM3_us(1600);
}

	
	
	