#include "low_power.h"

void Low_Power_Init()
{
	SCB->SCR |= 1<<2;  //进入深度睡眠模式
	RCC->APB1ENR |= 1<<28;   //开启PWR时钟
	PWR->CR |= 1<<1;   //深度睡眠掉电
	PWR->CR |= 1<<2;   //清WUF唤醒标志位
	PWR->CSR |= 1<<8;   //使能WKUP
	RTC->CRL &= ~(0X7F<<8);   //将所有RTC标志清零
	__WFI();  //唤醒中断指令
}	





