#include "ch340.h"
#include "stdio.h"
#include "bluetooth.h"
#include "w25q64.h"

/*************************
函数功能：串口初始化
说明：PA10---RX-----浮空输入或带上拉输入
      PA9----TX-----推挽复用输出
*************************/

void USART1_Init(u32 Bount)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;  // 定义结构体变量
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1,ENABLE);  //开A口时钟  串口1时钟
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;     //引脚9
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;  //推挽复用输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;  //输出速率为50MHZ
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;     //引脚10
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;  //浮空输入
	GPIO_Init(GPIOA,&GPIO_InitStruct);	
	
	//USART设置
	USART_InitStruct.USART_BaudRate = Bount;  //波特率
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;  //数据位8位
	USART_InitStruct.USART_StopBits = USART_StopBits_1;     //一个停止位
	USART_InitStruct.USART_Parity = USART_Parity_No;        //无奇偶模式
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;    //无硬件流控制
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;       //使能发送  接收
	USART_Init(USART1,&USART_InitStruct);

	//NVIC设置
	NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;            //设置中断源串口1
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;     //抢占优先级
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;           //响应优先级
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;            //使能NVIC
	NVIC_Init(&NVIC_InitStruct);
	
//	USART_ITConfig(USART1,USART_IT_IDLE,ENABLE);             //使能空闲中断
	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);            //使能串口1中断

//	USART_DMACmd(USART1,USART_DMAReq_Rx, ENABLE );       //使能接收DMA请求
	USART_Cmd(USART1,ENABLE);  //使能串口
}	

/********************** 串口中断服务函数 ************************************/
ZIKU_buff ziku_data;  
u8 wait = 0;   //开始接收标志   1为接收完成
u32 rec_num = 0;  //字库储存地址
u8 rec_sta =0;    //接收完成标志


void USART1_IRQHandler(void)
{
	u8 data = 0;
	static u16 i=0;
	static u8 group = 0;

	if(USART_GetITStatus(USART1,USART_IT_RXNE))
	{
		USART_ClearITPendingBit(USART1,USART_IT_RXNE);   //清标志
		if(mode == 0)
		{
			TIM_Cmd(TIM2,DISABLE );  //关闭定时器2
			TIM_SetCounter(TIM2, 0);
			receive_buff[receive_len] = USART_ReceiveData(USART1);
			receive_len++;
			blue_flag = 1;
			if(receive_len >= 20)
			{			
				receive_len=0;
			}
			TIM_Cmd(TIM2,ENABLE );  //开启定时器2
		}
		else if(mode == 1)
		{
			wait = 1;
			TIM_Cmd(TIM2,DISABLE );  //关闭定时器2
			TIM_SetCounter(TIM2, 0);   //计数器清零
			data = USART1->DR;
			W25Q64_Write_Page(rec_num++,1,&data);
			TIM_Cmd(TIM2,ENABLE );  //开启定时器2
		}
	}

}

/***************** 串口发送字符串函数 **************************/
void USART_1_Send_Str(u8 *Data)
{
	while( *Data != '\0')
	{
		USART_SendData(USART1,*Data);
		while( !USART_GetFlagStatus(USART1,USART_FLAG_TC));       //发送完成
		Data++;
	}
}


/**********************printf底层函数**********************************/
int fputc(int data,FILE * file)
{
	while( !USART_GetFlagStatus(USART1,USART_FLAG_TC))
	{
		//轮询是否发送完成
	}
	USART_SendData(USART1,data);
	return data;
}
