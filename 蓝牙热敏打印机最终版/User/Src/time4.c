#include "time4.h"
#include "lb1936.h"
#include "printer.h"


/************************************************
函数功能：定时器4初始化函数
函数参数：
************************************************/
void TIM4_Init()
{
	NVIC_InitTypeDef NVIC_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
	
	TIM_SetCounter(TIM4,0);     //计数器初始值
	TIM_TimeBaseInitStruct.TIM_Period = 1000-1;   //重装载值
	TIM_TimeBaseInitStruct.TIM_Prescaler =72-1;  //分频值
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数
	TIM_TimeBaseInit(TIM4,&TIM_TimeBaseInitStruct);
//	
//	TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);  //使能中断
//	
//	//NVIC
//	NVIC_InitStruct.NVIC_IRQChannel = TIM4_IRQn;  //定时器4中断
//	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;  //抢占优先级
//	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;  //响应优先级
//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;   //通道使能
//	NVIC_Init(&NVIC_InitStruct);
	TIM_Cmd(TIM4,DISABLE );  //失能模块
}

/************************中断服务函数**********************************/
//void TIM4_IRQHandler(void)
//{
//	static u32 num = 0;

//	if(TIM_GetITStatus(TIM4,TIM_IT_Update))  //判断是否中断
//	{
//		TIM_ClearITPendingBit(TIM4,TIM_IT_Update);    //请标志
//		SiPai();   //电机节拍
//		if(num % 2 == 0)   //偶数
//		{
//			Lat_data();   //锁存数据
//			Printer_left();    //左边加热
//			Printer_flag = 1;   //标志置1 更新下一行数据
//		}
//		else
//		{
//			Printer_right();  //右边加热
//		}
//		num++;
//		if(num >= 300)
//		{
//			num = 0;
//			TIM_Cmd(TIM4,DISABLE );  //关闭定时器4
//			Printer_off();   //加热关闭
//		}

//	}
//}