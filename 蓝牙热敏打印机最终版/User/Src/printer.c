#include "printer.h"
#include "time.h"
#include "lb1936.h"

//#include "tupian.h"
#include "bluetooth.h"
#include "w25q64.h"
#include "ch340.h"
#include "fontword.h"
#include "stdio.h"



u8 Printer_flag = 1;    //打印刷新标志
u8 buff[12*128]={0};     //用来存储从w25q64读取的汉字
/*******************************
函数功能：打印头初始化
说明：
STB1-6----PB15-10  输出
LAT-------PA8      输出
CLK-------PA11    输出
DI--------PA12    输出

*********************************/
void Printer_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct; //定义结果体变量
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);    //开A口时钟  B口时钟


	GPIO_InitStruct.GPIO_Pin =GPIO_Pin_8 | GPIO_Pin_11 | GPIO_Pin_12;  //选中管脚8 11  12
	GPIO_InitStruct.GPIO_Mode =GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度	
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 
	                           | GPIO_Pin_14 | GPIO_Pin_15;  //选中管脚10 11  12  13 14 15
	GPIO_InitStruct.GPIO_Mode =GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; //输出速度	
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	//初始状态不加热
	Printer_off();  

}

void Lat_data(void)   //锁存数据
{
	LAT_H;
	LAT_L;
	LAT_H;
}
void Printer_off()   //关闭加热
{
	STB1_L;
	STB2_L;
	STB3_L;
	STB4_L;
	STB5_L;
	STB6_L;
}	


void Printer_left()  //加热左边
{
	
	STB1_H;
	STB2_H;
	STB3_H;
	STB4_L;
	STB5_L;
	STB6_L;
	TIM3_us(800);
	Printer_off();
	M_Drive();
		
}
void Printer_right()   //加热右边
{
	
	STB1_L;
	STB2_L;
	STB3_L;
	STB4_H;
	STB5_H;
	STB6_H;
	TIM3_us(2600);
	Printer_off();
	M_Drive();
		
}

void PaintH(u8 *data)    //锁存数据  打印汉字
{		
		int i,j;
	  u8 databuf;
	Printer_off(); //停止加热头

		for(i=0;i<48;i++)
		{
			databuf = *data;
			for(j=0;j<8;j++)
			{
				CLK_L;
				TIM3_us(1);
				if(databuf & (0x80))   
				{
					DI_H;
				}					
				else
				{
					DI_L;
				}					
				databuf = databuf << 1;
				TIM3_us(1);
				CLK_H;
				TIM3_us(1);
				CLK_L;
			}
			data++;//指针右移 共移动48次
		}
		Painter();
}

void Painter(void)     //交替加热打印
{
	STB1_H;
	STB2_H;
	STB3_H;
	STB4_L;
	STB5_L;
	STB6_L;
	TIM3_us(3000);
	STB1_L;
	STB2_L;
	STB3_L;
	STB4_H;
	STB5_H;
	STB6_H;
	TIM3_us(3000);
	Printer_off();
	M_Drive();
}
void PaintZF(u8 data)    //锁存数据  打印字符
{
	  int j;
	  Printer_off(); //停止加热头
   	for(j=0;j<8;j++)
			{
				CLK_L;
				TIM3_us(1);
				if(data & (0x80) )
				{
					DI_H;
				}					
				else
				{
					DI_L;
				}					
				data = data << 1;
				TIM3_us(1);
				CLK_H;
				TIM3_us(1);
				CLK_L;
			}
}
void write_data_8bit(u8 dat)   //锁存数据  汉字与字符
{
	u8 i;
	CLK_H;//拉高电平
	for(i=0; i<8; i++)
	{
		CLK_L;
		DI = dat >> 7;           //先发高位

		dat <<= 1;
		CLK_H;				 //上升沿发送数据
	}
}
void PainterT(void)       //加热左边打印
{
	STB1_H;
	STB2_H;
	STB3_H;
	TIM3_us(2600);
	Printer_off();
	M_Drive();
}


void PaintClearHZ(void)     //清除储存过的数据
{
	int j,k;
	for(j=0 ;j<24 ;j++ )
	{
		for(k=0;k<48;k++)
		{
			HZbuf[j][k] = NULL;
		}
	}
}










