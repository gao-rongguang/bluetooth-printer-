#include "systick.h"
#include "stdio.h"




/******************************
函数功能：SysTick初始化
函数参数：
*******************************/
void SysTick_Init()
{
	
	SysTick->VAL  = 0 ;  //计数器清零
	SysTick->LOAD = 72000-1;   //1ms
	
	SysTick->CTRL |= 1<<2;  //使能内核时钟
	SysTick->CTRL |= 1<<1;  //使能中断
	SysTick->CTRL |= 1<<0;  //使能定时器

}
u32 sys_time = 0;  //全局变量的定义
u32 sys_time_all[2] = {0,0xffffffff};   //起始时间    结束时间

/*****************中断服务函数************************/
void SysTick_Handler(void)
{
	if(SysTick->CTRL & 1<<16)//判断是否是计数到0
	{
		sys_time++;
		sys_time_all[0]++;
		
	}
}